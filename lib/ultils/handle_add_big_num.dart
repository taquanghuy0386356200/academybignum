

const decimalZeroInAscii = 48;

class HandleAddBigNum {
  //reverse String
  static String reverseString({required String value}) {
    return value.split('').reversed.join('');
  }

  static String findSum(String str1, String str2)
  {
    //str2 len have to larger than str1
    if (str1.length> str2.length){
      String t = str1;
      str1 = str2;
      str2 = t;
    }

    // this var store sum
    String str = "";

    int n1 = str1.length, n2 = str2.length;

    str1=reverseString(value: str1);
    str2=reverseString(value: str2);

    int carry = 0;
    for (int i = 0; i < n1; i++)
    {
      int sum = (str1.codeUnitAt(i) - decimalZeroInAscii) +
          (str2.codeUnitAt(i) - decimalZeroInAscii) + carry;
      str += String.fromCharCode(sum % 10 + decimalZeroInAscii);

      // Calc
      carry = (sum / 10).floor();
    }

    // Add remaining digits of larger number
    for (int i = n1; i < n2; i++)
    {
      int sum = (str2.codeUnitAt(i) - decimalZeroInAscii) + carry;
      str += String.fromCharCode(sum % 10 + decimalZeroInAscii);
      carry = (sum / 10).floor();
    }

    // Add remaining carry
    if (carry > 0) {
      str += String.fromCharCode(carry + decimalZeroInAscii);
    } else {
      //nothing
    }

    // reverse resultant String
    str = reverseString(value: str);

    return str;
  }

}
