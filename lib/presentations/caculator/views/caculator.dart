import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fresher_acadamy_big_num/presentations/caculator/bloc/caculator_bloc.dart';
import 'package:fresher_acadamy_big_num/ultils/handle_add_big_num.dart';

class CaculatorScreen extends StatefulWidget {
  const CaculatorScreen({Key? key}) : super(key: key);

  @override
  State<CaculatorScreen> createState() => _CaculatorScreenState();
}

class _CaculatorScreenState extends State<CaculatorScreen> {
  late CaculatorBloc bloc;
  late TextEditingController txtFirstNum ;
  late TextEditingController txtSecondNum ;

  @override
  void initState() {
    super.initState();
    bloc = CaculatorBloc();
    txtFirstNum = TextEditingController();
    txtSecondNum = TextEditingController();
  }
  
  @override
  void dispose() {
    txtFirstNum.dispose();
    txtSecondNum.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Bài test')),
      body: Center(
        child: Column(
          children: [
            const Text('First Number:'),
            TextFormField(
              controller: txtFirstNum,
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
              ],
            ),
            const Text('Second Number:'),
            TextFormField(
              controller: txtSecondNum,
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ElevatedButton(
                  child: const Text('clear'),
                  onPressed: () {
                    setState(() {
                      setState(() {
                        txtFirstNum.text = '';
                        txtSecondNum.text = '';
                        bloc.result = '';
                      });
                    });
                  },
                ),
                ElevatedButton(
                  child: const Text('Sum'),
                  onPressed: () {
                    setState(() {
                      bloc.result = HandleAddBigNum.findSum(
                          txtFirstNum.text.trim(), txtSecondNum.text.trim());
                    });
                  },
                ),
              ],
            ),
            Text('This is result: ${bloc.result}'),
          ],
        ),
      ),
    );
  }
}
