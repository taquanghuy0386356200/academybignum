
import 'package:flutter/material.dart';
import 'package:fresher_acadamy_big_num/presentations/caculator/views/caculator.dart';

const String intro =
    'Em xin cảm ơn anh đã cho phép em làm bài test ạ, về phần testing thì em chưa viết và có kinh nghiệm về unitest, nếu có cơ hội được làm việc em hy vọng sẽ được học hỏi và tiếp xúc và phát triển ạ! Xin cảm ơn';

class IntroduceMySelf extends StatelessWidget {
  const IntroduceMySelf({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sample Code'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            const Text(intro),
            TextButton(
              style: TextButton.styleFrom(
                primary: Colors.blue,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (_) =>  const CaculatorScreen()),
                );
              },
              child: const Text('Bài test'),
            )
          ],
        ),
      ),
    );
  }
}
